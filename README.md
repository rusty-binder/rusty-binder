**_Please be aware that this crate is no longer actively maintained, if you wish to fork the project feel free to do so (and if you let me know I might even contribute every now and again)_**

# rusty-binder

[![Build Status](https://gitlab.com/rusty-binder/rusty-binder/badges/master/build.svg)](https://gitlab.com/rusty-binder/rusty-binder/builds)
[![crates.io](http://meritbadge.herokuapp.com/rusty-binder)](https://crates.io/crates/rusty-binder)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](http://mit-license.org/)

**This project is part of a major rewrite of
[rusty-cheddar](https://github.com/Sean1708/rusty-cheddar) with an aim to make a more
language-agnostic bindings generator. Until rusty-binder is put on `crates.io` it should be
considered entirely work-in-progress and unusable. Thanks for your patience!**

Rusty-binder attempts to provide a framework to generate language bindings from Rust files.

**A note on versioning:** While rusty-binder is still in a significant flux (i.e. pre-`v1.0.0`)
it will likely go through numerous breaking changes. However, until `v1.0.0`, any time a
breaking change is made the minor version will be bumped and any time a new feature is added
the path version will be bumped. If you find that this is not the case then please open an
issue at the [repo](https://gitlab.com/rusty-binder/rusty-binder).

Rusty-binder is a language-agnostic bindings generator which is designed to use
language-specific external crates in a build script. A typical use might look something like
this:

```toml
# Cargo.toml

[package]
build = "build.rs"

[build-dependencies]
rusty-binder = "..."
# NOTE THAT THESE TWO CRATES (`rusty-python` and `rusty-ruby`) HAVE NOT YET BEEN WRITTEN,
# THEY ARE JUST BEING USED AS HYPOTHETICAL EXAMPLES.
rusty-python = "..."
rusty-ruby = "..."

[lib]
crate-type = ["dylib"]
```

```rust
// build.rs

extern crate binder;
extern crate python;
extern crate ruby;

fn main() {
    let python = python::Generator(...);
    let ruby = ruby::Generator(...);

    binder::Binder::new().expect("could not read manifest")
        .module("capi").expect("malformed path")
        .output_directory("my_bindings")
        .register(python)
        .register(ruby)
        .run_build();
}
```

After this there would be Python bindings available in `my_bindings/python` and Ruby bindings
available in `my_bindings/ruby` (or locations similar to that).

## Contributing

Contributions to rusty-binder are more than welcome.

### Bugs

If you find a bug or have a feature request please open an issue. I can't guarantee that I'll fix it
but I'll give it a damn good go.

If you find the source code unclear in any way then I consider that a bug. I try to make my source
code as clear as possible but I'm not very good at it, so any help in that regard is appreciated.

### PRs

I love pull requests, they tend to make my job much easier, so if you want to fix a bug or implement
a feature yourself then that would be great. If you're confused by anything or need some pointers on
how to proceed then feel free to open an issue so that I can help, otherwise [these docs] are a good
place to start.

This project uses [clog] so please make sure your commit messages adhere to [conventional format],
thanks!

### New Languages

If you want to write a binding for your favourite language but don't really know where to start then
please open an issue on the [repo] so that I can give you a hand. If you've already written a
binding and you want it listed in this document or you want it to be part of the [rusty-binder]
group (don't worry, you'll still be the lead maintainer) then again open an issue on the [repo]
or submit a pull request and I'll be happy to sort that for you.

[these docs]: http://manishearth.github.io/rust-internals-docs/syntax/ast/index.html
[clog]: https://github.com/clog-tool/clog-cli
[conventional format]: https://github.com/ajoslin/conventional-changelog/blob/a5505865ff3dd710cf757f50530e73ef0ca641da/conventions/angular.md
[repo]: https://gitlab.com/rusty-binder/rusty-binder
[rusty-binder]: https://gitlab.com/rusty-binder
